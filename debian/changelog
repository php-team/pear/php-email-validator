php-email-validator (4.0.3-2) unstable; urgency=medium

  * Compatibility with recent PHPUnit (12)
  * Simplify build

 -- David Prévot <taffit@debian.org>  Fri, 14 Feb 2025 13:26:57 +0100

php-email-validator (4.0.3-1) unstable; urgency=medium

  [ Arnt Gulbrandsen ]
  * Regularise a test. (#389)

  [ Eduardo Gulias Davis ]
  * Update readme
  * Removed stale changelog

  [ David Prévot ]
  * Revert "Force system dependencies loading"
  * Drop old changelog

 -- David Prévot <taffit@debian.org>  Sun, 29 Dec 2024 11:19:09 +0100

php-email-validator (4.0.2-3) unstable; urgency=medium

  * Don’t use deprecated comma-separated values with --exclude-group
    (Closes: #1070539)
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sun, 19 May 2024 17:40:29 +0200

php-email-validator (4.0.2-2) unstable; urgency=medium

  * Upload to unstable
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Tue, 05 Mar 2024 12:24:18 +0100

php-email-validator (4.0.2-1) experimental; urgency=medium

  [ chris ]
  * chore: update phpunit 9 = 10 and psalm 4 = 5 (#368)
  * chore: add tests for php 8.3 (#373)

  [ Christian Rishøj ]
  * perform AAAA check separately (fixes #301) (#376)

  [ David Prévot ]
  * Extend clean

 -- David Prévot <taffit@debian.org>  Mon, 09 Oct 2023 13:23:45 +0200

php-email-validator (4.0.1-1) experimental; urgency=medium

  [ KergeKacsa ]
  * DNS record check now passes if email address has no top-level domain (#355)

 -- David Prévot <taffit@debian.org>  Wed, 18 Jan 2023 08:15:08 +0100

php-email-validator (4.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Bastien Wermeille ]
  * Upgrade to doctrine/lexer 3.x
  * loosen constraint over lexer

  [ Alexander M. Turek ]
  * Fix Lexer 2 compatibility (#349)

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Tue, 10 Jan 2023 07:21:35 +0100

php-email-validator (3.2.5-1) unstable; urgency=medium

  [ Dries Vints ]
  * GitHub Actions (#348)

 -- David Prévot <taffit@debian.org>  Mon, 02 Jan 2023 19:50:42 +0100

php-email-validator (3.2.4-1) unstable; urgency=medium

  [ Alexander M. Turek ]
  * Allow doctrine/lexer 2 (#345)

 -- David Prévot <taffit@debian.org>  Sat, 31 Dec 2022 08:11:42 +0100

php-email-validator (3.2.3-1) unstable; urgency=medium

  [ Eduardo Gulias Davis ]
  * Update LICENSE

  [ David Prévot ]
  * Update standards version to 4.6.2, no changes needed.
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Fri, 30 Dec 2022 12:52:01 +0100

php-email-validator (3.2.1-1) unstable; urgency=medium

  * Upload to unstable in sync with symfony

  [ Nicolas Grekas ]
  * Don't use utf8_en/decode() functions, they're deprecated on PHP 8.2 (#325)

  [ David Prévot ]
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Mon, 20 Jun 2022 08:10:54 +0200

php-email-validator (3.1.2-1) experimental; urgency=medium

  [ Samuel Vicent ]
  * MultipleErrors->reason always returns [0] and should return
    first item from associative array (#305)

  [ David Prévot ]
  * Update standards version to 4.6.0, no changes needed.

 -- David Prévot <taffit@debian.org>  Mon, 11 Oct 2021 22:46:16 -0400

php-email-validator (3.1.1-2) experimental; urgency=medium

  * Exclude slow test
  * Allow stderr during CI

 -- David Prévot <taffit@debian.org>  Thu, 08 Apr 2021 16:18:32 -0400

php-email-validator (3.1.1-1) experimental; urgency=medium

  [ Dries Vints ]
  * Add null return type to getError DocBlock (#293)

  [ John Congdon ]
  * Reset the label when a S_DOT is encountered. (#297)

 -- David Prévot <taffit@debian.org>  Thu, 08 Apr 2021 09:14:05 -0400

php-email-validator (3.1.0-1) experimental; urgency=medium

  [ Eduardo Gulias Davis ]
  * Update CHANGELOG.md
  * Message-id validator (#290)

  [ David Prévot ]
  * Fix CI (missing directory)
  * Simplify gbp import-orig
  * Use dh-sequence-phpcomposer instead of pkg-php-tools
  * Install /u/s/p/overrides file
  * Provide php-egulias-email-validator
  * Generate phpab templates at build time

 -- David Prévot <taffit@debian.org>  Tue, 09 Mar 2021 08:31:59 -0400

php-email-validator (3.0.0-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Eduardo Gulias Davis ]
  * Final info to changelog

  [ Kevin Krummnacker ]
  * Allow PHP 8 for the upcoming 3.0 (#275)

  [ David Prévot ]
  * Drop patches not needed anymore
  * Use vendor/autoload.php for tests
  * Use Internet during autopkgtest
  * Use now provided upstream changelog
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sat, 02 Jan 2021 13:03:26 -0400

php-email-validator (2.1.25-1) unstable; urgency=medium

  [ vajexal ]
  * Fix labels length check (#278)

 -- David Prévot <taffit@debian.org>  Sat, 02 Jan 2021 11:44:42 -0400

php-email-validator (2.1.24-2) unstable; urgency=medium

  * Adapt to recent version of PHPUnit (9)

 -- David Prévot <taffit@debian.org>  Sun, 13 Dec 2020 09:07:52 -0400

php-email-validator (2.1.24-1) unstable; urgency=medium

  [ David ]
  * Email addresses with single quote should not be valid (#268)

  [ David Prévot ]
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sat, 21 Nov 2020 16:01:17 -0400

php-email-validator (2.1.22-1) unstable; urgency=medium

  [ Eduardo Gulias Davis ]
  * fix #217 (#266)

 -- David Prévot <taffit@debian.org>  Mon, 28 Sep 2020 06:48:10 -0400

php-email-validator (2.1.21-1) unstable; urgency=medium

  [ kishor ]
  * Issue-257: Fix PHP 7.3 compatibility issues. (#264)

 -- David Prévot <taffit@debian.org>  Thu, 24 Sep 2020 08:49:07 -0400

php-email-validator (2.1.20-1) unstable; urgency=medium

  [ Eduardo Gulias Davis ]
  * Fix #260 with workaround (#261)

  [ Remi Collet ]
  * Fix paths for tests (#244)

  [ BenHarris ]
  * Reimplement DNSCheckValidation (#250)

  [ David Prévot ]
  * Set Rules-Requires-Root: no.
  * Rename main branch to debian/latest (DEP-14)
  * Add back failing tests that now pass
  * Comment out a test requesting network access

 -- David Prévot <taffit@debian.org>  Tue, 08 Sep 2020 14:36:29 -0400

php-email-validator (2.1.18-1) unstable; urgency=medium

  [ Simon Schaufelberger ]
  * Move files in typical src and tests folder (#242)

  [ David Prévot ]
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test
  * Adapt packaging to new layout
  * Adapt phpunit.xml.dist to new layout

 -- David Prévot <taffit@debian.org>  Mon, 29 Jun 2020 13:26:34 -1000

php-email-validator (2.1.17-1) unstable; urgency=medium

  [ Denis Ryabov ]
  * Fix "Trying to access array offset on value of type null" (#228)

  [ Marcin Michalski ]
  * Fix local part length check (#233)

 -- David Prévot <taffit@debian.org>  Fri, 14 Feb 2020 13:17:16 -1000

php-email-validator (2.1.15-1) unstable; urgency=medium

  * Update Standards-Version to 4.5.0
  * Replace polyfill-intl-idn dependency by php-intl

 -- David Prévot <taffit@debian.org>  Fri, 24 Jan 2020 16:38:14 -1000

php-email-validator (2.1.14-1) unstable; urgency=medium

  * New upstream release

 -- David Prévot <taffit@debian.org>  Sun, 12 Jan 2020 22:53:47 +1300

php-email-validator (2.1.13-1) unstable; urgency=medium

  [ Graham Campbell ]
  * Fixed tests (#223)

  [ David Prévot ]
  * Set upstream metadata fields: Bug-Submit.
  * Revert "Adpapt to new php-symfony-phpunit-bridge build-dependency"

 -- David Prévot <taffit@debian.org>  Sat, 04 Jan 2020 11:13:10 +1100

php-email-validator (2.1.12-1) unstable; urgency=medium

  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.
  * d/control: Drop versioned dependency satisfied in (old)stable

 -- David Prévot <taffit@debian.org>  Mon, 23 Dec 2019 04:24:22 +1100

php-email-validator (2.1.11-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Advertise return annotations from doctrine/lexer (#209)

 -- David Prévot <taffit@debian.org>  Thu, 15 Aug 2019 22:19:43 -1000

php-email-validator (2.1.10-2) unstable; urgency=medium

  * Set upstream metadata fields: Contact, Name.
  * Add ci dependency

 -- David Prévot <taffit@debian.org>  Sun, 04 Aug 2019 04:17:46 -0400

php-email-validator (2.1.10-1) unstable; urgency=medium

  * Upload to unstable since buster has been released

  [ Nicolas Grekas ]
  * Fix PHP 7.4 support (#203)

  [ David Prévot ]
  * Update Standards-Version to 4.4.0
  * Adpapt to new php-symfony-phpunit-bridge build-dependency

 -- David Prévot <taffit@debian.org>  Mon, 22 Jul 2019 11:53:21 -0300

php-email-validator (2.1.9-1) experimental; urgency=medium

  [ Eduardo Gulias Davis ]
  * fix(201) - empty domain

 -- David Prévot <taffit@debian.org>  Sun, 23 Jun 2019 16:32:15 -1000

php-email-validator (2.1.8-1) experimental; urgency=medium

  * Upload to experimental during the freeze
  * Document gbp import-ref usage
  * Add back testsuite
  * Use debhelper-compat 12
  * Update Standards-Version to 4.3.0

 -- David Prévot <taffit@debian.org>  Thu, 20 Jun 2019 08:36:40 -1000

php-email-validator (2.1.7-1) unstable; urgency=medium

  [ Eduardo Gulias Davis ]
  * Update supported RFCs (#186)

  [ Joël Harkes ]
  * Update MultipleValidationWithAnd.php (#179)

 -- David Prévot <taffit@debian.org>  Wed, 05 Dec 2018 06:56:27 -1000

php-email-validator (2.1.6-1) unstable; urgency=medium

  * Use debhelper-compat 11
  * Get rid of get-orig-source target
  * Use https in Format
  * Update Standards-Version to 4.2.1

 -- David Prévot <taffit@debian.org>  Thu, 27 Sep 2018 18:18:11 -1000

php-email-validator (2.1.5-1) unstable; urgency=medium

  [ Tom Sommer ]
  * DNSCheckValidation requires the Intl extension (#175)
  * Correctly check for intl extension in SpoofCheck (#174)
  * INTL_IDNA_VARIANT_UTS46 is not supported on all systems (#173)

  [ David Prévot ]
  * Update Standards-Version to 4.2.0

 -- David Prévot <taffit@debian.org>  Mon, 20 Aug 2018 17:09:13 -1000

php-email-validator (2.1.4-1) unstable; urgency=medium

  [ Lode Claassen ]
  * DNS check should convert unicode domains first (#166)

  [ David Prévot ]
  * Update Standards-Version to 4.1.4

 -- David Prévot <taffit@debian.org>  Mon, 16 Apr 2018 15:44:17 -1000

php-email-validator (2.1.3-2) unstable; urgency=medium

  * Upload to unstable now that symfony can be built with this version
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.3

 -- David Prévot <taffit@debian.org>  Thu, 08 Mar 2018 17:52:23 -1000

php-email-validator (2.1.3-1) experimental; urgency=medium

  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Sat, 18 Nov 2017 16:37:40 -1000

php-email-validator (2.1.2-1) experimental; urgency=medium

  * Drop minimal version satisfied in stable
  * Update Standards-Version to 4.1.0

 -- David Prévot <taffit@debian.org>  Tue, 22 Aug 2017 11:24:00 -1000

php-email-validator (2.0.1-1) experimental; urgency=medium

  [ Issei.M ]
  * fixed DNSCheckValidation bug (#115)
  * fixed some problems in DNSCheckValidation (#116)

  [ Michele Locati ]
  * Exclude some dirs and files from repository auto-generated ZIP archives

  [ David Prévot ]
  * Remove tests dropped upstream
  * Remove documentation dropped upstream

 -- David Prévot <taffit@debian.org>  Fri, 15 Jul 2016 10:49:37 -0400

php-email-validator (2.0.0-1) experimental; urgency=medium

  * Upload to experimental since symfony currently (as of 3.1) build-depend on
    php-email-validator (<< 2)

  [ Eduardo Gulias Davis ]
  * moving code around

  [ David Prévot ]
  * Revert "Track version 1"
  * Update packaging to new distribution path
  * Update copyright (years)
  * Build-depend on php-intl that provides Spoofchecker needed for tests
  * Drop test relying on network access

 -- David Prévot <taffit@debian.org>  Thu, 19 May 2016 22:25:22 -0400

php-email-validator (1.2.12-1) unstable; urgency=medium

  [ Eduardo Gulias Davis ]
  * Changed invalid dns domains and backslash in domain (#107)

  [ David Prévot ]
  * Track version 1
  * Update Standards-Version to 3.9.8

 -- David Prévot <taffit@debian.org>  Thu, 19 May 2016 21:18:16 -0400

php-email-validator (1.2.11-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7
  * Rebuild with latest pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Fri, 04 Mar 2016 22:33:42 -0400

php-email-validator (1.2.11-1) unstable; urgency=medium

  [ Eduardo Gulias Davis ]
  * Control tests for .com.xx domains
  * fixes #82

 -- David Prévot <taffit@debian.org>  Wed, 11 Nov 2015 11:15:42 -0400

php-email-validator (1.2.10-1) unstable; urgency=medium

  [ Andrei Sozonov ]
  * #80 validate number of closing parenthesis

 -- David Prévot <taffit@debian.org>  Fri, 23 Oct 2015 18:36:47 -0400

php-email-validator (1.2.9-1) unstable; urgency=medium

  [ Chris McCafferty ]
  * Replaces dos-type line endings. Fixes issue #69.

 -- David Prévot <taffit@debian.org>  Wed, 24 Jun 2015 10:03:32 -0400

php-email-validator (1.2.8-1) unstable; urgency=medium

  * The latest php-doctrine-lexer is needed
  * Provide homemade autoload.php

 -- David Prévot <taffit@debian.org>  Tue, 12 May 2015 15:09:28 -0400

php-email-validator (1.2.7-1) unstable; urgency=low

  * Initial release

 -- David Prévot <taffit@debian.org>  Thu, 08 Jan 2015 05:06:07 -0400
